insert into user_roles (role, description) VALUES ('ROLE_USER', 'default role for user');
insert into user_roles (role, description) VALUES ('ROLE_ADMIN', 'role for system administrators');