package pl.pawlakjakub.shareit.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import pl.pawlakjakub.shareit.model.Post;


public interface PostRepository extends PagingAndSortingRepository<Post, Long> {

    Page<Post> findAllByUser_IdOrderByRatingDesc(Long userId, Pageable pageRequest);
    Page<Post> findAllByUser_IdOrderByRatingAsc(Long userId, Pageable pageRequest);
    Page<Post> findAllByUser_IdOrderByPublishDateDesc(Long userId, Pageable pageRequest);
    Page<Post> findAllByUser_IdOrderByPublishDateAsc(Long userId, Pageable pageRequest);
}
