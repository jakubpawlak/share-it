package pl.pawlakjakub.shareit.repository;

import org.springframework.data.repository.CrudRepository;
import pl.pawlakjakub.shareit.model.UserRole;

public interface UserRoleRepository extends CrudRepository<UserRole, Long> {
    UserRole findByRole(UserRole.Role role);
}
