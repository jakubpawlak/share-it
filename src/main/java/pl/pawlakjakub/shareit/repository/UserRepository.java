package pl.pawlakjakub.shareit.repository;

import org.springframework.data.repository.CrudRepository;
import pl.pawlakjakub.shareit.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);
    User findByEmail(String email);

    boolean existsByUsername(String username);
    boolean existsByEmail(String email);
}
