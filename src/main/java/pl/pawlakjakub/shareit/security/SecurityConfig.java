package pl.pawlakjakub.shareit.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.thymeleaf.extras.springsecurity5.dialect.SpringSecurityDialect;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder() {
        PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        return passwordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                    .antMatchers("/").permitAll()
                    .antMatchers("/users/register").permitAll()
                    .antMatchers("/users/login").permitAll()
                    .antMatchers("/users/login/fail").permitAll()
                    .antMatchers("/users/register/success").permitAll()
                    .anyRequest().authenticated()
                .and()
                .formLogin()
                    .loginPage("/users/login").permitAll()
                    .loginProcessingUrl("/users/processlogin")
                    .usernameParameter("email")
                    .passwordParameter("password")
                    .failureUrl("/users/login/fail")
                .and()
                .logout()
                    .logoutUrl("/users/logout")
                    .logoutSuccessUrl("/");

    }

    @Bean
    public SpringSecurityDialect springSecurityDialect() {
        return new SpringSecurityDialect();
    }
}
