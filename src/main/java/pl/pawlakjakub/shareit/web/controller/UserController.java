package pl.pawlakjakub.shareit.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.pawlakjakub.shareit.model.forms.PasswordChangeForm;
import pl.pawlakjakub.shareit.model.forms.UserForm;
import pl.pawlakjakub.shareit.service.UserService;

import javax.validation.Valid;

@Controller
@RequestMapping("/users")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public String registerNewUser(@Valid @ModelAttribute UserForm userForm, BindingResult result) {
        if (result.hasErrors()) {
            return "registration";
        } else {
            userService.saveUserWithDefaultRole(userForm);
            return "redirect:/users/register/success";
        }
    }

    @GetMapping("/register")
    public String getRegistrationPage(Model model) {
        model.addAttribute("userForm", new UserForm());
        return "registration";
    }

    @GetMapping("/login")
    public String getLoginPage() {
        return "login";
    }

    @GetMapping("/login/fail")
    public String getFailedLoginPage() {
        return "loginfailed";
    }

    @GetMapping("/register/success")
    public String getSuccessfulRegistrationPage() {
        return "registration_success";
    }

    @GetMapping("/password_change")
    public String getPasswordChangePage(Model model) {
        model.addAttribute("passwordChangeForm", new PasswordChangeForm());
        return "password_change";
    }

    @PostMapping("/password_change")
    public String changePassword(@Valid @ModelAttribute PasswordChangeForm passwordChangeForm, BindingResult result) {
        if (result.hasErrors()) {
            return "password_change";
        } else {
            userService.changePassword(passwordChangeForm.getPassword());
            return "redirect:/users/password_change/success";
        }
    }

    @GetMapping("/password_change/success")
    public String getSuccessfulPasswordChangePage() {
        return "password_change_success";
    }

}
