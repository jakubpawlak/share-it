package pl.pawlakjakub.shareit.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.pawlakjakub.shareit.model.Post;
import pl.pawlakjakub.shareit.service.PostService;

@Controller
public class HomeRestController {

    private PostService postService;

    @Autowired
    public HomeRestController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping("/")
    public String index(Model model,
                        @RequestParam(defaultValue = "0") int page,
                        @RequestParam(defaultValue = "date") String sorting,
                        @RequestParam(defaultValue = "desc") String order) {

        Page<Post> postPage = postService.getAllPostsPage(page, sorting, order);

        int lastPageNumber = postPage.getTotalPages();

        model.addAttribute("allPosts", postPage);
        model.addAttribute("lastPageNumber", lastPageNumber);
        model.addAttribute("pageNumber", page);
        model.addAttribute("sorting", sorting);
        model.addAttribute("order", order);

        return "index";
    }
}
