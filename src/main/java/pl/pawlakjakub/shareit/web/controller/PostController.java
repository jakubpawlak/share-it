package pl.pawlakjakub.shareit.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.pawlakjakub.shareit.model.Post;
import pl.pawlakjakub.shareit.service.PostService;

import javax.validation.Valid;
import java.util.Arrays;

@Controller
@RequestMapping("/posts")
public class PostController {

    private PostService postService;

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping("/add")
    public String newPostForm(Model model) {
        model.addAttribute("post", new Post());
        return "add";
    }

    @GetMapping("/{id}")
    public String getPostVotes(@PathVariable Long id, Model model) {
        Post post = postService.getPost(id);
        model.addAttribute("allPosts", Arrays.asList(post));
        return "index :: post";
    }

    @PostMapping("/add")
    public String saveNewPost(@Valid @ModelAttribute Post post, BindingResult result) {
        if (result.hasErrors()) {
            return "add";
        } else {
            postService.save(post);
            return "redirect:/";
        }
    }

    @PutMapping("/{id}/upvote")
    @ResponseBody
    public void upvotePost(@PathVariable Long id) {
        postService.upvotePost(id);
    }

    @PutMapping("/{id}/downvote")
    @ResponseBody
    public void downvotePost(@PathVariable Long id) {
        postService.downvotePost(id);
    }

    @GetMapping("/my")
    public String getUserPostsPage(Model model,
                                   @RequestParam(defaultValue = "0") int page,
                                   @RequestParam(defaultValue = "date") String sorting,
                                   @RequestParam(defaultValue = "desc") String order) {

        Page<Post> userPostsPage = postService.getLoggedUserPostsPage(page, sorting, order);

        int lastPageNumber = userPostsPage.getTotalPages();

        model.addAttribute("userAllPosts", userPostsPage);
        model.addAttribute("lastPageNumber", lastPageNumber);
        model.addAttribute("pageNumber", page);
        model.addAttribute("sorting", sorting);
        model.addAttribute("order", order);

        return "user_posts";
    }
}
