package pl.pawlakjakub.shareit.model;

import lombok.*;
import pl.pawlakjakub.shareit.model.forms.UserForm;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity(name = "users")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long id;

    @NotNull
    @Size(min = 4, max = 20)
    private String username;

    @NotNull
    @Size(min = 8)
    private String password;

    @NotNull
    @Email
    private String email;

    @ManyToOne
    @JoinColumn(name = "user_role_id")
    private UserRole role;

    @OneToMany(mappedBy = "user")
    private List<Post> posts;

    public static User fromUserForm(UserForm form) {
        User user = new User();

        user.setUsername(form.getUsername());
        user.setPassword(form.getPassword());
        user.setEmail(form.getEmail());

        return user;
    }

}
