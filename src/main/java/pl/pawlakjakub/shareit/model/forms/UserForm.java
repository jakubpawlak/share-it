package pl.pawlakjakub.shareit.model.forms;

import lombok.*;
import pl.pawlakjakub.shareit.validation.email.constraint.UniqueEmail;
import pl.pawlakjakub.shareit.validation.username.constraint.UniqueUsername;
import pl.pawlakjakub.shareit.validation.password.constraint.RepeatedPassword;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@RepeatedPassword(message = "{pl.pawlakjakub.shareit.model.forms.UserForm.RepeatedPassword}")
public class UserForm implements PasswordConfirmation {

    @NotNull
    @Size(min = 4, max = 20, message = "{pl.pawlakjakub.shareit.model.forms.UserForm.username.Size}")
    @UniqueUsername(message = "{pl.pawlakjakub.shareit.model.forms.UserForm.username.UniqueUsername}")
    private String username;

    @NotNull
    @Size(min = 8, message = "{pl.pawlakjakub.shareit.model.forms.UserForm.password.Size}")
    private String password;

    @NotNull
    private String repeatedPassword;

    @NotNull
    @Email(message = "{pl.pawlakjakub.shareit.model.forms.UserForm.email.Email}")
    @UniqueEmail(message = "{pl.pawlakjakub.shareit.model.forms.UserForm.email.UniqueEmail}")
    private String email;
}
