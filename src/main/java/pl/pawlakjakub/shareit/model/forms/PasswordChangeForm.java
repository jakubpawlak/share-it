package pl.pawlakjakub.shareit.model.forms;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.pawlakjakub.shareit.validation.password.change.constraint.OldPassword;
import pl.pawlakjakub.shareit.validation.password.constraint.RepeatedPassword;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@RepeatedPassword(message = "{pl.pawlakjakub.shareit.model.forms.UserForm.RepeatedPassword}")
public class PasswordChangeForm implements PasswordConfirmation {

    @OldPassword(message = "{pl.pawlakjakub.shareit.model.forms.PasswordChangeForm.OldPassword}")
    private String oldPassword;

    @NotNull
    @Size(min = 8)
    private String password;

    @NotNull
    private String repeatedPassword;
}
