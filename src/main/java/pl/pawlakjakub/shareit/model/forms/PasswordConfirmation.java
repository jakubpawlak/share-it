package pl.pawlakjakub.shareit.model.forms;

public interface PasswordConfirmation {
    String getPassword();
    String getRepeatedPassword();
}
