package pl.pawlakjakub.shareit.model;

import lombok.*;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDateTime;

@Entity(name = "posts")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @NotEmpty
    @URL(message = "{pl.pawlakjakub.shareit.model.Post.link.URL}")
    private String link;


    private LocalDateTime publishDate;

    @NotEmpty
    @Size(max = 500, message = "{pl.pawlakjakub.shareit.model.Post.description.NotEmpty}")
    private String description;

    @NotEmpty(message = "{pl.pawlakjakub.shareit.model.Post.title.NotEmpty}")
    @Size(max = 255, message = "{pl.pawlakjakub.shareit.model.Post.title.Size}")
    private String title;

    private int rating = 0;

}
