package pl.pawlakjakub.shareit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.pawlakjakub.shareit.model.Post;
import pl.pawlakjakub.shareit.model.User;
import pl.pawlakjakub.shareit.repository.PostRepository;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class PostService {
    private PostRepository postRepository;
    private UserService userService;
    private static final int PAGE_SIZE = 10;

    @Autowired
    public PostService(PostRepository postRepository, UserService userService) {
        this.postRepository = postRepository;
        this.userService = userService;
    }

    public void save(Post post) {
        User loggedUser = userService.getLoggedUser();
        post.setUser(loggedUser);
        post.setPublishDate(LocalDateTime.now());
        postRepository.save(post);
    }

    public Page<Post> getAllPostsPage(int pageNumber, String sorting, String order) {
        Page<Post> postsPage;
        PageRequest pageRequest;

        if (sorting.equals("rating")) {
            if (order.equals("asc")) {
                pageRequest = PageRequest.of(pageNumber, PAGE_SIZE, Sort.by("rating").ascending());
            } else {
                pageRequest = PageRequest.of(pageNumber, PAGE_SIZE, Sort.by("rating").descending());
            }
        } else {
            if (order.equals("asc")) {
                pageRequest = PageRequest.of(pageNumber, PAGE_SIZE, Sort.by("publishDate").ascending());
            } else {
                pageRequest = PageRequest.of(pageNumber, PAGE_SIZE, Sort.by("publishDate").descending());
            }
        }

        postsPage = postRepository.findAll(pageRequest);

        return postsPage;
    }

    public List<Post> getSortedByRatingDescending(int pageNumber) {
        List<Post> posts = new LinkedList<>();
        PageRequest pageRequest = PageRequest.of(pageNumber, PAGE_SIZE, Sort.by("rating").descending());
        postRepository.findAll(pageRequest).forEach(posts::add);

        return posts;
    }

    public List<Post> getSortedByRatingAscending(int pageNumber) {
        List<Post> posts = new LinkedList<>();
        PageRequest pageRequest = PageRequest.of(pageNumber, PAGE_SIZE, Sort.by("rating").ascending());
        postRepository.findAll(pageRequest).forEach(posts::add);

        return posts;
    }

    public List<Post> getSortedByDateDescending(int pageNumber) {
        List<Post> posts = new LinkedList<>();
        PageRequest pageRequest = PageRequest.of(pageNumber, PAGE_SIZE, Sort.by("publishDate").descending());
        postRepository.findAll(pageRequest).forEach(posts::add);

        return posts;
    }

    public List<Post> getSortedByDateAscending(int pageNumber) {
        List<Post> posts = new LinkedList<>();
        PageRequest pageRequest = PageRequest.of(pageNumber, PAGE_SIZE, Sort.by("publishDate").ascending());
        postRepository.findAll(pageRequest).forEach(posts::add);

        return posts;
    }

    public void upvotePost(Long id) {
        Optional<Post> optionalPost = postRepository.findById(id);
        if (optionalPost.isPresent()) {
            Post post = optionalPost.get();
            post.setRating(post.getRating() + 1);
            postRepository.save(post);
        }
    }

    public void downvotePost(Long id) {
        Optional<Post> optionalPost = postRepository.findById(id);
        if (optionalPost.isPresent()) {
            Post post = optionalPost.get();
            post.setRating(post.getRating() - 1);
            postRepository.save(post);
        }
    }

    public Post getPost(Long id) {
        Optional<Post> optionalPost = postRepository.findById(id);
        if (optionalPost.isPresent()) {
            Post post = optionalPost.get();
            return post;
        }
        return null;
    }

    public Page<Post> getLoggedUserPostsPage(int page, String sorting, String order) {
        User loggedUser = userService.getLoggedUser();
        PageRequest pageRequest = PageRequest.of(page, PAGE_SIZE);

        Page<Post> postsPage;

        if (sorting.equals("rating")) {
            if (order.equals("asc")) {
                postsPage = postRepository.findAllByUser_IdOrderByRatingAsc(loggedUser.getId(), pageRequest);
            } else {
                postsPage = postRepository.findAllByUser_IdOrderByRatingDesc(loggedUser.getId(), pageRequest);
            }
        } else {
            if (order.equals("asc")) {
                postsPage = postRepository.findAllByUser_IdOrderByPublishDateAsc(loggedUser.getId(), pageRequest);
            } else {
                postsPage = postRepository.findAllByUser_IdOrderByPublishDateDesc(loggedUser.getId(), pageRequest);
            }
        }

        return postsPage;
    }
}
