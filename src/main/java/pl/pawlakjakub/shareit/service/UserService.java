package pl.pawlakjakub.shareit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.pawlakjakub.shareit.model.User;
import pl.pawlakjakub.shareit.model.forms.UserForm;
import pl.pawlakjakub.shareit.model.UserRole;
import pl.pawlakjakub.shareit.repository.UserRepository;
import pl.pawlakjakub.shareit.repository.UserRoleRepository;

import java.util.Optional;

@Service
public class UserService {

    private PasswordEncoder passwordEncoder;
    private UserRepository userRepository;
    private UserRoleRepository userRoleRepository;

    private static final UserRole.Role DEFAULT_ROLE = UserRole.Role.ROLE_USER;

    @Autowired
    public UserService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setUserRoleRepository(UserRoleRepository userRoleRepository) {
        this.userRoleRepository = userRoleRepository;
    }

    public User findUserByEmail(String email) {
        User userByEmail = userRepository.findByEmail(email);
        return userByEmail;
    }

    public void saveUserWithDefaultRole(UserForm userForm) {
        UserRole defaultRole = userRoleRepository.findByRole(DEFAULT_ROLE);

        User user = User.fromUserForm(userForm);
        user.setRole(defaultRole);

        String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);

        userRepository.save(user);
    }

    public User getLoggedUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User loggedUser = null;

        if (principal instanceof UserDetails) {
            String email = ((UserDetails) principal).getUsername();
            loggedUser = userRepository.findByEmail(email);
        }

        return loggedUser;
    }

    public void changePassword(String newPassword) {
        User loggedUser = getLoggedUser();

        String newPasswordEncoded = passwordEncoder.encode(newPassword);
        loggedUser.setPassword(newPasswordEncoded);

        userRepository.save(loggedUser);
    }
}
