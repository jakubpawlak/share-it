package pl.pawlakjakub.shareit.validation.username;

import org.springframework.beans.factory.annotation.Autowired;
import pl.pawlakjakub.shareit.repository.UserRepository;
import pl.pawlakjakub.shareit.validation.username.constraint.UniqueUsername;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueUsernameValidator implements ConstraintValidator<UniqueUsername, String> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public boolean isValid(String username, ConstraintValidatorContext context) {
        boolean usernameTaken = userRepository.existsByUsername(username);
        return !usernameTaken;
    }

    @Override
    public void initialize(UniqueUsername constraintAnnotation) {

    }
}
