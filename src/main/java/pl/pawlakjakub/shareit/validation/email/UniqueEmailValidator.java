package pl.pawlakjakub.shareit.validation.email;

import org.springframework.beans.factory.annotation.Autowired;
import pl.pawlakjakub.shareit.repository.UserRepository;
import pl.pawlakjakub.shareit.validation.email.constraint.UniqueEmail;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueEmailValidator implements ConstraintValidator<UniqueEmail, String> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void initialize(UniqueEmail constraintAnnotation) {

    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext constraintValidatorContext) {
        boolean exists = userRepository.existsByEmail(email);
        return !exists;
    }
}
