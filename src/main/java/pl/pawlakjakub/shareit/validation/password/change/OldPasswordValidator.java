package pl.pawlakjakub.shareit.validation.password.change;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import pl.pawlakjakub.shareit.model.User;
import pl.pawlakjakub.shareit.service.UserService;
import pl.pawlakjakub.shareit.validation.password.change.constraint.OldPassword;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class OldPasswordValidator implements ConstraintValidator<OldPassword, String> {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public boolean isValid(String oldPasswordRaw, ConstraintValidatorContext constraintValidatorContext) {
        User loggedUser = userService.getLoggedUser();
        String oldPasswordEncoded = loggedUser.getPassword();

        return passwordEncoder.matches(oldPasswordRaw, oldPasswordEncoded);
    }
}
