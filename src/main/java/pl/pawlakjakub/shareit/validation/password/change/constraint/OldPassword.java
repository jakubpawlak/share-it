package pl.pawlakjakub.shareit.validation.password.change.constraint;

import pl.pawlakjakub.shareit.validation.password.change.OldPasswordValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.ElementType.TYPE;

@Documented
@Constraint(validatedBy = {OldPasswordValidator.class})
@Target({METHOD, FIELD, CONSTRUCTOR, PARAMETER, ANNOTATION_TYPE, TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface OldPassword {
    String message() default "Old password is incorrect";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
