package pl.pawlakjakub.shareit.validation.password;

import pl.pawlakjakub.shareit.model.forms.PasswordConfirmation;
import pl.pawlakjakub.shareit.validation.password.constraint.RepeatedPassword;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RepeatedPasswordValidator implements ConstraintValidator<RepeatedPassword, PasswordConfirmation> {
    @Override
    public boolean isValid(PasswordConfirmation passwordConfirmationForm, ConstraintValidatorContext context) {
        String password = passwordConfirmationForm.getPassword();
        String repeatedPassword = passwordConfirmationForm.getRepeatedPassword();

        boolean equals = password.equals(repeatedPassword);

        if (!equals) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
                    .addPropertyNode("repeatedPassword")
                    .addConstraintViolation();
        }

        return equals;
    }

    @Override
    public void initialize(RepeatedPassword constraintAnnotation) {

    }
}
